module Puppet::Parser::Functions
	newfunction(:keyfact2string, :type => :rvalue) do |args|
		# parse an args hash
		raise Puppet::ParseError, "Sshkeys: Must supply a Hash to keyfact2string(), not a #{args[0].class}" unless args[0].is_a? Hash
	
		args_keys = args[0]
		key_string = ''
		
		args_keys.each do |name, attrs|
			next unless attrs.key?('sshkey_root::type')
			next unless attrs.key?('sshkey_root::key')
			
			key_string += attrs['sshkey_root::type'] + ' ' + attrs['sshkey_root::key'] + ' ' + name + "\n"
		end
		
		return key_string
	end
end
