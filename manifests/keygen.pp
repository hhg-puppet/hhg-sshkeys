# == Resource: sshkeys::keygen
#
# See README.md for details
#
# === Authors
#
# Sebastian Rettenberger <rettenberger.sebastian@arcor.de>
#
# === Copyright
#
# Copyright 2016 Sebastian Rettenberger
#

define sshkeys::keygen (
	$user = root,
	$home = '/root',
	$type = rsa,
	
) {
	if ! ($user) {
		$user = $name
	}
	
	validate_string($user)
	validate_string($home)
	validate_string($type)
	
	if ! ($type in ['dsa', 'ecdsa', 'rsa']) {
		fail("unsupported key type ${type}")
	}
	
	$filename = "${home}/.ssh/id_${type}"
	
	exec { "ssh-keygen ${user}":,
		command => "/usr/bin/ssh-keygen -q -t ${type} -f ${filename} -P ''",
		user => $user,
		creates => $filename,
		require => File["${home}/.ssh/"]
	}
}
