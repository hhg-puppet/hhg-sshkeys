# sshkeys

#### Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with sshkeys](#setup)
    * [What sshkeys affects](#what-sshkeys-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with sshkeys](#beginning-with-sshkeys)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
6. [Limitations - OS compatibility, etc.](#limitations)

## Overview

This module configures sshkeys for the root user.

## Module Description

This modules generates the sshkey for root and installs it on all systems.
It is designed for server farms where the master users wants to be able to log in everywhere.

## Setup

### What sshkeys affects

* Private/public key pair of root
* Authorized key file of root
* Global known hosts file

### Setup Requirements

Requires Foreman to for full functionality.

### Beginning with sshkeys

See [Usage](#usage)

## Usage

### Using default values

	class { '::sshkeys': }

### Using available parameters

	class { '::sshkeys':
		generate_key => true,
		foreman_host => 'foreman.mydomin',
		foreman_user => 'query',
		foreman_pass => 'mypass' }
		
## Reference

### Public Classes

* sshkeys: Main class, includes all other classes.

### Private Classes

* sshkeys::authorized_keys: Manages root's authorized_keys file
* sshkeys::known_hosts: Manages known_hossts file

### Private Resources

* sshkeys::keygen: Generates sshkeys.

### Parameters

#### `generate_key`

Enable/disable generation of the private/public key pair for root.

#### `authorized_keys`

A string containing keys (one per line) that should be included in the root's authorized_keys file.

#### `managed_keys`

Enable/disable the inclusion of managed keys in the authorized_keys file.

#### `known_hosts`

Enable/disable managing of known hosts.

#### `foreman_host`

The Foreman host that should queried for ssh key facts.

#### `foreman_user`

The Foreman user that should be used for querying ssh key facts.

#### `foreman_pass`

The password for the Foreman user.

### Facts

* sshkey_root: The public ssh key for root

## Limitations

This module has been built on and tested against Puppet 4.

The module has beent tested on Debian Jessie.
