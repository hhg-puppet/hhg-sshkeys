module Puppet::Parser::Functions
	newfunction(:hostfact2knownhost, :type => :rvalue) do |args|
		# parse an args hash
		raise Puppet::ParseError, "Sshkeys: Must supply a Hash to hostfact2knownhost(), not a #{args[0].class}" unless args[0].is_a? Hash

		args_keys = args[0]
		known_hosts = Hash.new

		args_keys.each do |name, attrs|
			next unless attrs.key?('sshecdsakey')

			host_aliases = []
			if attrs.key?('hostname')
				host_aliases << attrs['hostname']
			end
			if attrs.key?('ipaddress')
				host_aliases << attrs['ipaddress']
			end

			known_hosts[name] = { :type => 'ecdsa-sha2-nistp256',
				:key => attrs['sshecdsakey'],
				:host_aliases => host_aliases }
		end

		return known_hosts
	end
end
