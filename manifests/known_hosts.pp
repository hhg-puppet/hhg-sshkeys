# == Class: sshkeys::known_hosts
#
# See README.md for details
#
# === Authors
#
# Sebastian Rettenberger <rettenberger.sebastian@arcor.de>
#
# === Copyright
#
# Copyright 2016 Sebastian Rettenberger
#

class sshkeys::known_hosts inherits sshkeys
{
	$hosts_from_foreman = foreman({item => 'fact_values',
			search => 'fact = hostname or fact = ipaddress or fact = sshecdsakey',
			per_page => 10000,
			foreman_url => "https://${sshkeys::foreman_host}/",
			foreman_user => $sshkeys::foreman_user,
			foreman_pass => $sshkeys::foreman_pass})
	$known_hosts_raw = $hosts_from_foreman[results]
	$known_hosts = hostfact2knownhost($known_hosts_raw)

	ensure_resources('sshkey', $known_hosts)
}
