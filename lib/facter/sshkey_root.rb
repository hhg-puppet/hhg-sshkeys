# == Fact: sshkey_root
#
# See README.md for details
#
# === Authors
#
# Sebastian Rettenberger <rettenberger.sebastian@arcor.de>
#
# === Copyright
#
# Copyright 2016 Sebastian Rettenberger
#

Facter.add(:sshkey_root) do
	confine :kernel => 'Linux'
	
	setcode do
		# TODO support other types
		if File.exist? '/root/.ssh/id_rsa.pub'
			components = File.read('/root/.ssh/id_rsa.pub').split
			
			sshkey_root = {
				'type' => components[0],
				'key' => components[1],
				'server' => components[2]
			}
			
			sshkey_root
		else
			nil
		end
	end
end
