# == Class: sshkeys::authorized_keys
#
# See README.md for details
#
# === Authors
#
# Sebastian Rettenberger <rettenberger.sebastian@arcor.de>
#
# === Copyright
#
# Copyright 2016 Sebastian Rettenberger
#

class sshkeys::authorized_keys inherits sshkeys
{
	$authorized_keys_file = '/root/.ssh/authorized_keys'
	
	# Workaround for Foreman using Windows newlines
	$authorized_keys_unix = regsubst($sshkeys::authorized_keys, '\r\n', "\n", 'G')
	
	# Keys from facts
	if ($sshkeys::managed_keys) {
		$keys_from_foreman = foreman({item => 'fact_values',
			search => 'fact = sshkey_root::type or fact = sshkey_root::key',
			per_page => 10000,
			foreman_url => "https://${sshkeys::foreman_host}/",
			foreman_user => $sshkeys::foreman_user,
			foreman_pass => $sshkeys::foreman_pass})
		$other_keys = $keys_from_foreman[results]
		
		$other_key_string = keyfact2string($other_keys)
	} else {
		$other_key_string = ''
	}
	
	# Create authorized_keys file
	file{ $authorized_keys_file:
		ensure => 'present',
		owner => 'root',
		group => 'root',
		mode => '0600',
		content => "# MANAGED BY PUPPET!\n\n${authorized_keys_unix}\n\n#Keys from other hosts\n${other_key_string}",
		require => File['/root/.ssh/']
	}
}
