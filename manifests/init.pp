# == Class: sshkeys
#
# See README.md for details
#
# === Authors
#
# Sebastian Rettenberger <rettenberger.sebastian@arcor.de>
#
# === Copyright
#
# Copyright 2016 Sebastian Rettenberger
#
class sshkeys
(
	$generate_key = true,
	$authorized_keys = '',
	$managed_keys = true,
	$known_hosts = true,
	$foreman_host = $servername,
	$foreman_user = query,
	$foreman_pass = ''
)

{
	# Check parameters
	validate_bool($generate_key)
	validate_string($authorized_keys)
	validate_bool($managed_keys)
	validate_bool($known_hosts)
	validate_string($foreman_url)
	validate_string($foreman_user)
	validate_string($foreman_pass)
	
	# Generate .ssh directory for root
	ensure_resource('file',
		'/root/.ssh/',
		{ 'ensure' => 'directory',
		  'owner' => 'root',
		  'group' => 'root',
		  'mode' => '0700' })
	
	# Generate key
	if ($generate_key) {
		sshkeys::keygen{ 'root': }
	}
	
	# Generate authorized_keys
	include sshkeys::authorized_keys
	
	# Manage known hosts
	if ($known_hosts) {
		include sshkeys::known_hosts
	}
}
